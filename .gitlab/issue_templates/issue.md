**Description**

Describe the issue or bug you are experiencing.

**Steps To Reproduce**

List the steps to reproduce the issue.

**Screenshots**

Include any screenshots that are relevant, or delete this section.

**Environment**

Server type (Bukkit/Spigot/Paper): 

Server version: 

Java version: 

Plugin version: 

**Additional Details / Logs**

Any other information you can share

Please include any log messages you received when experiencing the issue.
