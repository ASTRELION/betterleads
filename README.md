<div align="center">

<img src="assets/logo.png" height="100" />

[![Latest Release](https://gitlab.com/ASTRELION/betterleads/-/badges/release.svg)](https://gitlab.com/ASTRELION/betterleads/-/releases)
[![pipeline status](https://gitlab.com/ASTRELION/betterleads/badges/main/pipeline.svg)](https://gitlab.com/ASTRELION/betterleads/-/commits/main)
[![Spigot Downloads](https://img.shields.io/spiget/downloads/87512?color=%23F7C539&label=Spigot%20Downloads)](https://spigotmc.org/resources/launchme.87512)
[![Spigot Rating](https://img.shields.io/spiget/stars/87512?color=F7C539&label=Spigot%20Rating)](https://spigotmc.org/resources/launchme.87512/reviews)
[![bStats Servers](https://img.shields.io/bstats/servers/15788?color=00695C)](https://bstats.org/plugin/bukkit/BetterLeads/15788)
[![bStats Servers](https://img.shields.io/bstats/players/15788?color=00695C)](https://bstats.org/plugin/bukkit/BetterLeads/15788)

</div>

# BetterLeads

BetterLeads is a Minecraft plugin that improves the functionality of leads, making them less frustrating and more useful.

## Features

- Unbreakable leads
- Teleport leashed mobs to you when they get too far
- Apply potion effects to leashed mobs for better transit
- Leashed mobs can follow through dimensions and when teleporting
- Make virtually ANY mob leashable (including Villagers)!
- [HIGHLY configurable!](#configuration)
- [Permissable](#permissions)
- Open-source :)

## Server Admins

### Usage

All features are enabled by default, just make sure you have the correct [permissions](#permissions).

Leash a mob and...
- It will have quality of life potion effects (speed, jump boost) applied to it so that the mob will follow you better
- It is much harder to kill (by default, 80% damage resistance)
- Its lead won't break unless you break it manually
- It will teleport to you if it is too far away/stuck
- It will teleport to you if you teleport or switch dimensions

You may also configure usually-not-leashable mobs to be leashable! By default, this includes Villagers!

All features are configurable in `config.yml`. 

### Configuration

[The default configuration can be found here.](src/main/resources/config.yml)

A copy of the default configuration will be generated at `plugins/BetterLeads/config.yml` once you've added the plugin `.jar` and restarted or reloaded.

### Permissions

| Permission                        | Description                                                                                                                                                                                                 |
|-----------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `betterleads.*`                   | *Grants all permissions below*                                                                                                                                                                              |
| `betterleads.help`                | Allows the player to use `/betterleads` and `/betterleads help`                                                                                                                                             |
| `betterleads.leash.*`             | Allows the player to leash any mob listed in `config.yml`, and grants `betterleads.use.vanilla`                                                                                                             |
| `betterleads.leash.vanilla`       | Allows the player to leash all [vanilla/default leashable mobs](https://minecraft.fandom.com/wiki/Lead#Leashing_mobs). **Granted by default to all players**.                                               |
| `betterleads.leash.<entity_type>` | Allows the player to leash the given [`EntityType`](https://jd.papermc.io/paper/1.19/org/bukkit/entity/EntityType.html), e.g. `betterleads.use.VILLAGER`. `EntityType` must also be present in `config.yml` |
| `betterleads.reload`              | Allows the player to use `/betterleads reload`                                                                                                                                                              |
| `betterleads.update`              | Players with this permission will receive update alerts                                                                                                                                                     |
| `betterleads.use`                 | Allows the player to use the quality of life features of Better Leads (unbreakable leads, potion effects, teleportation)                                                                                    |
| `betterleads.version`             | Allows the player to use `/betterleads version`                                                                                                                                                             |

### Commands

| Command                 | Permission            | Description                                                         |
|-------------------------|-----------------------|---------------------------------------------------------------------|
| `/betterleads` or `/bl` | `betterleads.help`    | Same as `/betterleads help`                                         |
| `/betterleads help`     | `betterleads.help`    | Show help dialog                                                    |
| `/betterleads reload`   | `betterleads.reload`  | Reload `config.yml` and `lang.yml` from file                        |
| `/betterleads version`  | `betterleasd.version` | Shows Better Leads' version and if there is a new version available |

## Developers

## Statistics

[<img src="https://bstats.org/signatures/bukkit/BetterLeads.svg" height="250" />](https://bstats.org/plugin/bukkit/BetterLeads/15788)

## Donate

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/astrelion)
