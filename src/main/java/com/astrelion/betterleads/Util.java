package com.astrelion.betterleads;

import com.astrelion.astrelplugin.util.AstrelUtil;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class Util extends AstrelUtil
{
    /**
     * Gets the entities that are leashed to the Player.
     * @param player the Player
     * @return List of Entity that are leashed to the Player
     */
    public static List<Entity> getLeashedEntities(Player player)
    {
        return player.getNearbyEntities(10, 10, 10)
            .stream()
            .filter(e -> e instanceof LivingEntity le &&
                le.isLeashed() &&
                le.getLeashHolder() == player)
            .collect(Collectors.toList());
    }
}
