package com.astrelion.betterleads;

import com.astrelion.astrelplugin.AstrelPermissions;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

public class Permissions extends AstrelPermissions
{
    public static final String LEASH = "betterleads.leash";
    public static final String LEASH_ALL = "betterleads.leash.*";
    public static final String LEASH_VANILLA = "betterleads.leash.VANILLA";
    public static final String USE = "betterleads.use";

    private final BetterLeads plugin;
    private final Configuration configuration;

    public Permissions(BetterLeads plugin)
    {
        super(plugin);
        this.plugin = plugin;
        this.configuration = plugin.getConfiguration();
    }

    /**
     * Determine if the given user uses Better Leads.
     * @param player the Player to check
     * @return true if the player uses better leads, false otherwise
     */
    public boolean usesBetterLeads(Player player)
    {
        return player.hasPermission(USE);
    }

    /**
     * Determine if the given player can leash the given entity. This method checks whether the player
     * has the regular betterleads.use node as well as the betterleads.use.[entity_type] node.
     * @param player the Player
     * @param entity the LivingEntity
     * @return true if the player can leash the entity, false otherwise
     */
    public boolean canLeash(Player player, LivingEntity entity)
    {
        // check for vanilla leash
        if (configuration.isVanillaLeashable(entity) && player.hasPermission(LEASH_VANILLA))
        {
            return true;
        }

        return configuration.isLeashable(entity) &&
            (player.hasPermission(LEASH + "." + entity.getType()) || player.hasPermission(LEASH_ALL));
    }
}
