package com.astrelion.betterleads;

import com.astrelion.astrelplugin.AstrelUpdateManager;
import com.astrelion.astrelplugin.commands.AstrelCommand;
import com.astrelion.astrelplugin.AstrelPlugin;

/**
 * BetterLeads by ASTRELION.
 * Makes leads easier to handle by increasing leashed mob speed, unbreakable leads,
 * and other quality of life changes.
 */
public final class BetterLeads extends AstrelPlugin
{
    private Events events;

    public BetterLeads()
    {
        super(
            "https://gitlab.com/ASTRELION/betterleads/-/releases/permalink/latest",
            15788
        );
    }

    @Override
    public void onEnable()
    {
        onEnableBefore();

        language = new Language(this);
        configuration = new Configuration(this);
        permissions = new Permissions(this);

        updateManager = new AstrelUpdateManager(this);
        updateManager.sendUpdateAlert();

        if (Configuration.ENABLE)
        {
            events = new Events(this);
            commandManager.getCommandReplacements().addReplacement("base", "betterleads|bl");
            commandManager.registerCommand(new AstrelCommand(this));
        }

        onEnableAfter();
    }

    @Override
    public void onDisable()
    {
        onDisableBefore();
        events = null;
        onDisableAfter();
    }

    @Override
    public Configuration getConfiguration()
    {
        return (Configuration) configuration;
    }

    @Override
    public Language getLanguage()
    {
        return (Language) language;
    }

    @Override
    public Permissions getPermissions()
    {
        return (Permissions) permissions;
    }

    @Override
    public AstrelUpdateManager getUpdateManager()
    {
        return updateManager;
    }
}
